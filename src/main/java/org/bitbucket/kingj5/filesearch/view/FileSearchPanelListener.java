package org.bitbucket.kingj5.filesearch.view;

import java.io.File;

/**
 * This interface should be implemented by the container that contains the FileSearchPanel (likely a dialog).
 * It is mainly used to receive the events from the use of the Open and Cancel buttons.
 * <p>
 * The implementer should register themselves as a listener with the FileSearchPanel.
 */
public interface FileSearchPanelListener {

	/**
	 * Inform the listener that a file was selected, and provides that file
	 *
	 * @param file The file that was selected
	 */
	void handleFileSelected(File file);

	/**
	 * Inform the listener that the cancel button was selected, and that the containing window should be closed.
	 */
	void handleCancelSelected();
}
