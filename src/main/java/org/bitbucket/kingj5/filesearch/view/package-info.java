/**
 * Contains classes for implementing a view for the File Search suite, including a reference panel and dialog
 */
package org.bitbucket.kingj5.filesearch.view;