package org.bitbucket.kingj5.filesearch.view;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.io.File;

import javax.swing.JDialog;

/**
 * The FileSearchDialog creates a dialog containing the {@link FileSearchPanel}.  It also listens for
 * events from the panel indicating whether a file has been selected or if the dialog
 * has requested that it be closed.
 */
public class FileSearchDialog extends JDialog implements FileSearchPanelListener {

	/**
	 * File that was selected , if one was selected
	 */
	private File selectedFile = null;

	/**
	 * Creates a dialog with no owner, title or modality
	 */
	public FileSearchDialog() {
		super();
	}

	/**
	 * Creates a dialog with an owner, title and modality
	 *
	 * @param owner The frame that is the owner
	 * @param title The title to display at the top of the dialog
	 * @param modal Whether the dialog is modal or not
	 */
	public FileSearchDialog(Frame owner, String title, boolean modal) {
		super(owner, title, modal);
	}

	/**
	 * Creates a dialog with an owner, title and modality
	 *
	 * @param owner The dialog that is the owner
	 * @param title The title to display at the top of the dialog
	 * @param modal Whether the dialog is modal or not
	 */
	public FileSearchDialog(Dialog owner, String title, boolean modal) {
		super(owner, title, modal);
	}

	/**
	 * Creates a dialog with an owner, title and modality
	 *
	 * @param owner The window that is the owner
	 * @param title The title to display at the top of the dialog
	 * @param modal Whether the dialog is modal or not
	 */
	public FileSearchDialog(Window owner, String title, Dialog.ModalityType modal) {
		super(owner, title, modal);
	}

	/**
	 * Adds the view component to the dialog, and registers this dialog as a listener
	 *
	 * @param component Panel to be added to the dialog
	 */
	public void addView(FileSearchPanel component) {
		add(component);
		component.registerPanelListener(this);
	}

	/**
	 * Set the dialog as visible
	 */
	public void setVisible() {
		setVisible(true);
	}

	@Override
	public void handleFileSelected(File file) {
		this.selectedFile = file;
		dispose();
	}

	@Override
	public void handleCancelSelected() {
		dispose();
	}

	/**
	 * Gets the selected file from the GUI
	 *
	 * @return The selected file from the GUI, or null if no file was selected (e.g., the cancel button was clicked)
	 */
	public File getFile() {
		return this.selectedFile;
	}
}
