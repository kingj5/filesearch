package org.bitbucket.kingj5.filesearch.search;

import java.io.File;
import java.io.RandomAccessFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A file searching implementation using the Java Regular Expressions package.
 */
public class RegexSearch extends AbstractFileSearchAlgorithm {

	/**
	 * The search term compiled into a {@link Pattern}
	 */
	private Pattern searchTerm;

	/**
	 * Buffer size for which to search the file by.
	 */
	private final int BUFFER_SIZE = 16384;

	/**
	 * Property to store whether the search pattern is a valid regular expression.
	 * If this is false, the search cannot be performed.
	 */
	protected boolean regexValid;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * Compiles the search term into a {@link Pattern} for use during searching.
	 * If the compilation fails, an error will be returned to the GUI and the search ended.
	 *
	 * @param searchTerm The specified term/regular expression for which to search for
	 */
	@Override
	public void setSearchTerm(String searchTerm) {
		try {
			if (!isCaseSensitive())
				this.searchTerm = Pattern.compile(searchTerm, Pattern.CASE_INSENSITIVE);
			else
				this.searchTerm = Pattern.compile(searchTerm);
			regexValid = true;
		} catch (Exception e) {
			regexValid = false;
			logger.error("Error compiling regular expression: " + e.getMessage(), e);
			notifySearchError("Invalid Regex: " + e.getMessage());
		}
	}

	/**
	 * Executes the search.  This checks if the regular expression is valid prior to
	 * executing the search on the file name and/or file contents.
	 */
	@Override
	public void runSearch() {
		intermediateFiles.clear();
		getListOfFilteredFiles().clear();
		cancelSearch = false;
		if (!regexValid)
			return;

		try {
			logger.info("Executing search via RegexSearch");
			for (File file : getListOfFiles()) {
				if (cancelSearch)
					return;
				if (isSearchFilename()) {
					searchFileName(file);
				}
				if (isSearchFileContents()) {
					searchFileContents(file);
				}
			}
		} catch (Exception e) {
			logger.error("Error encountered during search: " + e.getMessage(), e);
			notifySearchError(e.getMessage());
			return;
		}
		logger.debug("Search completed");

		finalizeFileList();

		notifySearchCompleted();

	}

	/**
	 * Searches the file name for the given regular expression.
	 *
	 * @param file File to be searched
	 */
	protected void searchFileName(File file) {
		logger.trace("Searching filename [" + file.getName() + "] for match of [" + searchTerm.pattern() + "]");
		Matcher matcher = searchTerm.matcher(file.getName());
		if (matcher.find()) {
			synchronized (intermediateFiles) {
				logger.trace("Search matched for filename [" + file.getName() + "], adding to intermediate list.");
				intermediateFiles.add(file);
			}
		}
	}

	/**
	 * Searches the file contents for the given regular expression.
	 * <p>
	 * This searches the file in chunks, defined by the buffer size.
	 * If the file is longer than the buffer size, a sliding window of 2k
	 * will be used to provide enough overlap so that a search won't fail
	 * if a valid finding is split between buffers.
	 * </p>
	 *
	 * @param file The file to be searched
	 * @throws Exception Thrown if there is an error while reading the file
	 */
	protected void searchFileContents(File file) throws Exception {
		logger.trace("Searching file contents of [" + file.getName() + "] for match of [" + searchTerm.pattern() + "]");
		byte[] buffer = new byte[BUFFER_SIZE];
		int read = 0;
		try (RandomAccessFile raf = new RandomAccessFile(file, "r")) {
			while ((read = raf.read(buffer)) != -1) {
				logger.trace("Read " + read + " bytes into buffer.");
				String data = new String(buffer, 0, read);

				Matcher match = searchTerm.matcher(data);

				if (match.find()) {
					logger.debug("Match found in file, creating FilteredFile");
					synchronized (intermediateFiles) {
						if (intermediateFiles.contains(file))
							intermediateFiles.remove(file);
					}
					FilteredFile filteredFile = new FilteredFile(file, createDescription(match, data));
					getListOfFilteredFiles().add(filteredFile);
					return;
				}

				if (read == buffer.length) {
					raf.seek(raf.getFilePointer() - 2048);
					logger.trace("No match found but more data available, adjusting sliding window.");
				}

			}

		}
		logger.trace("No match found in file.");
	}

	/**
	 * Creates a description to be used in a {@link FilteredFile} using
	 * the finding
	 *
	 * @param matcher Matcher containing information on the regex match
	 * @param data    The data that was searched by the matcher
	 * @return Description that can be used in a {@link FilteredFile}
	 */
	protected String createDescription(Matcher matcher, String data) {
		StringBuilder sb = new StringBuilder();
		int beginIndex;
		if (matcher.start() < 30) {
			beginIndex = 0;
		} else {
			beginIndex = matcher.start() - 30;
			sb.append("...");
		}
		if (data.substring(matcher.end()).length() < 30) {
			String tmpLine = data.substring(beginIndex);
			tmpLine = tmpLine.replaceAll("\n", "");
			sb.append(tmpLine);
		} else {
			String tmpLine = data.substring(beginIndex, matcher.end() + 30);
			tmpLine = tmpLine.replaceAll("\n", " ");
			sb.append(tmpLine);
			sb.append("...");
		}
		return sb.toString();
	}

}
