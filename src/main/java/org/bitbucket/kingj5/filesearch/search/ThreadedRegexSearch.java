package org.bitbucket.kingj5.filesearch.search;

import java.io.File;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A multi-threaded version of {@link RegexSearch} which can search multiple files at once.
 */
public class ThreadedRegexSearch extends RegexSearch {

	/**
	 * Number of threads to be used when searching
	 */
	private int numOfThreads;

	/**
	 * Queue of files left to be searched
	 */
	private ConcurrentLinkedQueue<File> filesQueue = new ConcurrentLinkedQueue<>();

	/**
	 * Latch to be used to determine when all threads have completed searching
	 */
	private CountDownLatch latch;

	/**
	 * Logger for class
	 */
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * Initializes the search algorithm with the given number of threads
	 *
	 * @param numOfThreads Number of threads to use when searching
	 */
	public ThreadedRegexSearch(int numOfThreads) {
		super();
		this.numOfThreads = numOfThreads;
	}

	/**
	 * Executes the search.  This checks if the regular expression is valid prior to
	 * spinning off the previously given number of threads to perform the search
	 * on the file name and/or file contents.  This method will wait until all
	 * threads have completed searching prior to returning.
	 */
	@Override
	public void runSearch() {

		if (!regexValid)
			return;
		cancelSearch = false;
		getListOfFilteredFiles().clear();
		intermediateFiles.clear();
		filesQueue.clear();

		filesQueue.addAll(getListOfFiles());
		ExecutorService service = Executors.newFixedThreadPool(numOfThreads);
		latch = new CountDownLatch(numOfThreads);
		logger.info("Executing search via ThreadedRegexSearch using " + numOfThreads + " threads");
		for (int i = 0; i < numOfThreads; i++) {
			service.submit(() -> searchFiles());
		}
		try {
			latch.await();
		} catch (InterruptedException e) {
		}
		logger.debug("Search completed");
		service.shutdown();
		if (cancelSearch)
			return;
		finalizeFileList();

		notifySearchCompleted();
	}

	/**
	 * Runnable that defines how to perform the search.
	 * Each thread will be running this task.
	 * <p>
	 * Each thread will continue to take files off of the queue and search
	 * them until the queue is empty, at which point each thread will signal
	 * that it has completed by toggling the countdown latch.
	 * </p>
	 * <p>
	 * If an error is encountered when searching a file, that file will be skipped
	 * and the thread will continue on to the next file.
	 * </p>
	 */
	private void searchFiles() {
		File file = filesQueue.poll();
		while (file != null && !cancelSearch) {
			try {
				if (isSearchFilename()) {
					searchFileName(file);
				}
				if (isSearchFileContents()) {
					searchFileContents(file);
				}
			} catch (Exception e) {
				//skip file?
				logger.error("Error searching file [" + file.getName() + "]: " + e.getMessage(), e);
			}
			file = filesQueue.poll();

		}
		//Queue is empty or cancelled
		latch.countDown();

	}
}
