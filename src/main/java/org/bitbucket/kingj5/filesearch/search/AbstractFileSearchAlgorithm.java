package org.bitbucket.kingj5.filesearch.search;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A base for File search algorithms to build on, this holds most of the boilerplate
 * that would be common between all algorithms.
 */
public abstract class AbstractFileSearchAlgorithm implements FileSearchAlgorithm {

	/**
	 * List of callback listeners to receive events when a search has completed or errored.
	 */
	private List<FileSearchListener> listeners = new ArrayList<>();

	/**
	 * List of files to be searched
	 */
	private List<File> searchFiles = new ArrayList<>();

	/**
	 * List of files that were searched and matched the search criteria
	 */
	private List<FilteredFile> filteredFiles = new ArrayList<>();

	/**
	 * Files that matched the criteria for title, but have not been added to the other list as they will
	 * still be searched for content too.
	 */
	protected List<File> intermediateFiles = new ArrayList<>();

	/**
	 * The criteria to search for
	 */
	private String searchTerm;

	/**
	 * Property defining whether or not to search filenames
	 */
	private boolean searchFilename;

	/**
	 * Property defining whether or not to search file contents
	 */
	private boolean searchFileContents;

	/**
	 * Property defining whether or not a search should be performed in a case sensitive manner
	 */
	private boolean caseSensitive;

	/**
	 * Property that defines whether files contained in directories that reside under the initial
	 * directory should be searched
	 */
	private boolean recursiveSearch;

	/**
	 * Boolean used to end a search should the cancel button on the GUI be clicked.
	 */
	protected boolean cancelSearch = false;

	/**
	 * Logger for class
	 */
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public void addListener(FileSearchListener listener) {
		synchronized (this.listeners) {
			this.listeners.add(listener);
		}
	}

	@Override
	public void removeListener(FileSearchListener listener) {
		synchronized (this.listeners) {
			this.listeners.remove(listener);
		}
	}

	@Override
	public void setListOfFiles(List<FilteredFile> files) {
		this.searchFiles.clear();

		//Just the files in this directory
		if (!recursiveSearch) {
			logger.debug("Only adding files in this directory.");
			this.searchFiles = files.stream().map(FilteredFile::getFile).filter(file -> !file.isDirectory())
					.collect(Collectors.toList());
		} else {
			logger.debug("Adding files by recursion, including files in directories under selected dir");
			this.searchFiles = populateListByRecursion(
					files.stream().map(FilteredFile::getFile).collect(Collectors.toList()));
		}

	}

	/**
	 * Recursively traverses the files contained in a directory and adds
	 * files contained underneath directories found in that directory
	 *
	 * @param files List of files in the current directory
	 * @return A full list of files to be searched that includes all files
	 * in directories underneath the current directory in addition to files
	 * in the current directory
	 */
	private List<File> populateListByRecursion(List<File> files) {
		List<File> filesToAdd = new ArrayList<>();
		for (File file : files) {
			if (file.isDirectory()) {
				logger.trace(file + " is a directory, adding all files underneath it");
				List<File> tmpList = new ArrayList<>();
				tmpList.addAll(Arrays.asList(file.listFiles()));
				filesToAdd.addAll(populateListByRecursion(tmpList));
			} else {
				logger.trace("Adding file to file list");
				filesToAdd.add(file);
			}
		}
		return filesToAdd;
	}

	/**
	 * Returns the list of files to be searched
	 *
	 * @return List of files to be searched
	 */
	protected List<File> getListOfFiles() {
		return this.searchFiles;
	}

	@Override
	public List<FilteredFile> getListOfFilteredFiles() {
		return this.filteredFiles;
	}

	@Override
	public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
	}

	@Override
	public String getSearchTerm() {
		return this.searchTerm;
	}

	@Override
	public void setSearchFilename(boolean search) {
		this.searchFilename = search;
	}

	@Override
	public void setSearchFileContents(boolean search) {
		this.searchFileContents = search;
	}

	@Override
	public void setCaseSensitive(boolean caseSensitive) {
		this.caseSensitive = caseSensitive;
	}

	@Override
	public void setRecursiveSearch(boolean recursiveSearch) {
		this.recursiveSearch = recursiveSearch;
	}

	@Override
	public void cancelSearch() {
		this.cancelSearch = true;
	}

	/**
	 * Gets the search filename property
	 *
	 * @return True if filenames should be searched, false otherwise
	 */
	protected boolean isSearchFilename() {
		return searchFilename;
	}

	/**
	 * Gets the search file contents property
	 *
	 * @return True if filenames should be searched, false otherwise
	 */
	protected boolean isSearchFileContents() {
		return searchFileContents;
	}

	/**
	 * Gets the case sensitive search property
	 *
	 * @return True if a search should be case sensitive, false otherwise.
	 */
	protected boolean isCaseSensitive() {
		return this.caseSensitive;
	}

	/**
	 * Notify any listeners that the search has completed successfully
	 */
	protected void notifySearchCompleted() {
		synchronized (this.listeners) {
			for (FileSearchListener l : this.listeners) {
				l.searchCompleted();
			}
		}
	}

	/**
	 * Notify any listeners that the search failed, with the given error string
	 *
	 * @param message String containing an error message as to why the search failed
	 */
	protected void notifySearchError(String message) {
		synchronized (this.listeners) {
			for (FileSearchListener l : this.listeners) {
				l.searchError(message);
			}
		}
	}

	/**
	 * Finalizes the search results. This makes filtered files from those found only in the title.
	 * If only searching by filename, this should be all files.
	 * If only searching by file contents, this should be empty.
	 */
	protected void finalizeFileList() {
		for (File file : intermediateFiles) {
			if (!isSearchFileContents())
				getListOfFilteredFiles().add(new FilteredFile(file, "Only searched file names"));
			else
				getListOfFilteredFiles().add(new FilteredFile(file, "Search term not found in file contents"));
		}
	}

}
