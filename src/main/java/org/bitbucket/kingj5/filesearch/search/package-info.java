/**
 * Contains various search algorithms for searching files and their contents.
 * <p>
 * All search algorithms are expected to implement the {@link org.bitbucket.kingj5.filesearch.search.FileSearchAlgorithm} interface
 * to be compatible with the existing framework
 */
package org.bitbucket.kingj5.filesearch.search;