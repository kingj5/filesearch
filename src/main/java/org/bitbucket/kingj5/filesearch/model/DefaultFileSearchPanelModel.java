package org.bitbucket.kingj5.filesearch.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.bitbucket.kingj5.filesearch.search.FileSearchAlgorithm;
import org.bitbucket.kingj5.filesearch.search.FileSearchListener;
import org.bitbucket.kingj5.filesearch.search.FilteredFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A default implementation of the {@link FileSearchPanelModel} interface which is
 * used in the reference implementation of FileSearch
 */
public class DefaultFileSearchPanelModel implements FileSearchPanelModel, FileSearchListener {

	/**
	 * Directory to search in
	 */
	private File directory;

	/**
	 * Search term to use while searching
	 */
	private String searchTerm;

	/**
	 * Boolean defining whether to search by filename
	 */
	private boolean searchFilename;

	/**
	 * Boolean defining whether to search by file contents
	 */
	private boolean searchFileContents;

	/**
	 * Boolean whether to use a case sensitive search
	 */
	private boolean caseSensitive;

	/**
	 * Boolean whether to do a recursive search that includes files in directories under this directory
	 */
	private boolean recursiveSearch;

	/**
	 * List of all files in the current directory
	 */
	private List<FilteredFile> filesList = new ArrayList<>();

	/**
	 * List of files that were found as a result of the search
	 */
	private List<FilteredFile> filteredFilesList = new ArrayList<>();

	/**
	 * File search algorithm to use for searching
	 */
	private FileSearchAlgorithm algorithm;

	/**
	 * Callback listeners
	 */
	private List<FileSearchPanelModelListener> listeners = new ArrayList<>();

	/**
	 * Executor service used during searching
	 */
	private ExecutorService service = Executors.newSingleThreadExecutor();

	/**
	 * Logger for class
	 */
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public void addListener(FileSearchPanelModelListener listener) {
		synchronized (this.listeners) {
			this.listeners.add(listener);
		}
	}

	@Override
	public void removeListener(FileSearchPanelModelListener listener) {
		synchronized (this.listeners) {
			this.listeners.remove(listener);
		}
	}

	@Override
	public void setDirectory(File directory) {
		this.directory = directory;
		populateFileList();
	}

	@Override
	public void setDirectory(String directory) {
		this.directory = new File(directory);
		populateFileList();
	}

	/**
	 * Populates the file list for all files in a directory
	 */
	private void populateFileList() {
		this.filesList.clear();
		if (this.directory != null) {
			for (File file : this.directory.listFiles()) {
				if (file.isDirectory()) {
					logger.trace("Adding file " + file.getName() + " as directory to filelist.");
					addFilteredFileToList(file, "Is directory; No search performed");
				} else {
					logger.trace("Adding file " + file.getName() + " to filelist.");
					addFilteredFileToList(file, "No search performed");
				}
			}
		}
		notifyDirectoryPopulationCompleted();
	}

	/**
	 * Creates a filtered file with the given description
	 */
	private void addFilteredFileToList(File file, String description) {
		FilteredFile filteredFile = new FilteredFile(file, description);
		this.filesList.add(filteredFile);
	}

	@Override
	public String getDirectoryAsString() {
		return this.directory.getName();
	}

	@Override
	public File getDirectoryAsFile() {
		return this.directory;
	}

	@Override
	public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
	}

	@Override
	public List<FilteredFile> getListOfFiles() {
		return this.filesList;
	}

	@Override
	public List<FilteredFile> getFilteredListOfFiles() {
		return this.filteredFilesList;
	}

	@Override
	public void setSearchFilename(boolean search) {
		this.searchFilename = search;
	}

	@Override
	public boolean getSearchFilename() {
		return this.searchFilename;
	}

	@Override
	public void setSearchFileContents(boolean search) {
		this.searchFileContents = search;
	}

	@Override
	public boolean getSearchFileContents() {
		return this.searchFileContents;
	}

	@Override
	public void setCaseSensitive(boolean caseSensitive) {
		this.caseSensitive = caseSensitive;
	}

	@Override
	public boolean getCaseSensitive() {
		return this.caseSensitive;
	}

	@Override
	public void setRecursiveSearch(boolean recursiveSearch) {
		this.recursiveSearch = recursiveSearch;
	}

	@Override
	public boolean getRecursiveSearch() {
		return this.recursiveSearch;
	}

	@Override
	public void setSearchAlgorithm(FileSearchAlgorithm algorithm) {
		this.algorithm = algorithm;
		this.algorithm.addListener(this);
	}

	@Override
	public void runSearch() {
		this.algorithm.setSearchFilename(this.searchFilename);
		this.algorithm.setSearchFileContents(this.searchFileContents);
		this.algorithm.setCaseSensitive(this.caseSensitive);
		this.algorithm.setRecursiveSearch(this.recursiveSearch);
		this.algorithm.setSearchTerm(this.searchTerm);
		this.algorithm.setListOfFiles(this.filesList);
		logger.info("Starting search on " + this.directory.getAbsolutePath() + "with options searchFilename ["
				+ this.searchFilename + "], searchFileContents {" + this.searchFileContents + "],"
				+ " caseSensitiveSearch [" + this.caseSensitive + "]," + " recursiveSearch [" + this.recursiveSearch
				+ "]");
		service.submit(() -> this.algorithm.runSearch());
	}

	@Override
	public void cancelSearch() {
		logger.debug("User requested cancel search");
		this.algorithm.cancelSearch();
	}

	@Override
	public void searchCompleted() {
		this.filteredFilesList.clear();
		this.filteredFilesList.addAll(this.algorithm.getListOfFilteredFiles());
		notifySearchCompleted();

	}

	@Override
	public void searchError(String message) {
		notifySearchFailed(message);

	}

	/**
	 * Notifies the listeners that the seasch has completed
	 */
	protected void notifySearchCompleted() {
		synchronized (this.listeners) {
			for (FileSearchPanelModelListener l : listeners) {
				l.searchCompleted();
			}
		}
	}

	/**
	 * Notifies the listeners that the search failed, and provides a reason why to be reported to the user.
	 *
	 * @param message Message containing the error that was encountered
	 */
	protected void notifySearchFailed(String message) {
		synchronized (this.listeners) {
			for (FileSearchPanelModelListener l : listeners) {
				l.searchFailed(message);
			}
		}
	}

	/**
	 * Notifies the listeners that the directory population has been completed.
	 */
	protected void notifyDirectoryPopulationCompleted() {
		synchronized (this.listeners) {
			for (FileSearchPanelModelListener l : listeners) {
				l.directoryChanged();
			}
		}
	}
}
