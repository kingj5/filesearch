package org.bitbucket.kingj5.filesearch.controller;

import java.io.File;
import java.util.List;

import org.bitbucket.kingj5.filesearch.model.FileSearchPanelModel;
import org.bitbucket.kingj5.filesearch.search.FilteredFile;

/**
 * The FileSearchPanelController is the base interface for any File Search Panel controller implementation.
 * It coordinates communication between the panel and the underlying model for the panel.
 */
public interface FileSearchPanelController {

	/**
	 * Sets the model to communicate with
	 *
	 * @param model Model to communicate with
	 */
	void setModel(FileSearchPanelModel model);

	/**
	 * Register a callback listener
	 *
	 * @param listener Callback listener
	 */
	void addListener(FileSearchPanelControllerListener listener);

	/**
	 * Unregister a previously registered callback listener
	 *
	 * @param listener Callback listener to be unregistered.
	 */
	void removeListener(FileSearchPanelControllerListener listener);

	/**
	 * Set the directory to be searched; the directory should be specified as a file
	 *
	 * @param directory Directory to be searched
	 */
	void setDirectory(File directory);

	/**
	 * Set the directory to be searched; the directory should be specified as a string and will
	 * be transformed into a file
	 *
	 * @param directory Directory to be searched
	 */
	void setDirectory(String directory);

	/**
	 * Return the current search directory as a string
	 *
	 * @return Current search directory
	 */
	String getDirectoryAsString();

	/**
	 * Return the current search directory as a File
	 *
	 * @return Current search directory
	 */
	File getDirectoryAsFile();

	/**
	 * Set the search expression to be used
	 *
	 * @param searchTerm Search expression to be used during searching
	 */
	void setSearchTerm(String searchTerm);

	/**
	 * Gets a list of files within the directory.  These files have no search applied to them
	 *
	 * @return List containing all files within search directory
	 */
	List<FilteredFile> getListOfFiles();

	/**
	 * Gets a list of filtered files.  A file is considered filtered if it has passed the
	 * search criteria and should be shown as a search result
	 *
	 * @return List containing all files meeting the search criteria in the directory.
	 */
	List<FilteredFile> getFilteredListOfFiles();

	/**
	 * Sets the search filename property.
	 *
	 * @param search True if filenames should be searched, false otherwise
	 */
	void setSearchFilename(boolean search);

	/**
	 * Gets the search filename property
	 *
	 * @return True if filenames will be searched, false otherwise
	 */
	boolean getSearchFilename();

	/**
	 * Sets the search file contents property
	 *
	 * @param search True if file contents should be searched, false otherwise
	 */
	void setSearchFileContents(boolean search);

	/**
	 * Gets the search file contents property
	 *
	 * @return True if file contents should be searched, false otherwise
	 */
	boolean getSearchFileContents();

	/**
	 * Sets the case sensitive search property
	 *
	 * @param caseSensitive True if search is case sensitive, false otherwise
	 */
	void setCaseSensitive(boolean caseSensitive);

	/**
	 * Gets the case sensitive search property
	 *
	 * @return True if search is case sensitive, false otherwise
	 */
	boolean getCaseSensitive();

	/**
	 * Sets the recursive search property
	 *
	 * @param recursiveSearch True if search should be recursive, false otherwise
	 */
	void setRecursiveSearch(boolean recursiveSearch);

	/**
	 * Gets the recursive search property
	 *
	 * @return True if search should be recursive, false otherwise
	 */
	boolean getRecursiveSearch();

	/**
	 * Executes the search using the given search term and search options
	 */
	void runSearch();

	/**
	 * Cancels a currently running search if one is in progress
	 */
	void cancelSearch();

}
